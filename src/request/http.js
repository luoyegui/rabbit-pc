import axios from "./index"
export function get(url, params) {
    return new Promise((resolve, reject) => {
        axios.get(url, {
            data: params
        }).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}
export function deletes(url, params) {
    return new Promise((resolve, reject) => {
        axios.delete(url, {
            data: params
        }).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}
export let post = (url, params) => {
    return new Promise((resolve, reject) => {
        axios.post(url, params).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err);
        })
    })
}