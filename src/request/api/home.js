import { get, post } from "../http"
export let getBannerAPI = (params) => get("/home/banner", params)
// 新鲜好物的接口
export let geNewAPI = (params) => get("/home/new", params)
// 人气推荐的接口
export let geHotAPI = (params) => get("/home/hot", params)
// 热门品牌的接口
export let geBrandAPI = (params) => get("/home/brand", params)
// 产品的接口
export let getGoodsAPI = (params) => get("/home/goods", params)
// 分类
export let getCategoryAPI = (params) => get("/home/category/head", params)
// 分类
export let getDetailAPI = (params) => get(" /goods", params)

