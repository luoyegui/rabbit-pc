//1、引入axios
import axios from "axios";

//2、添加axios的默认属性，请求超时时间
axios.defaults.timeout = 5000;
//3、判断当前环境，根据当前环境进行接口基本地址的设置
// 开发环境 /api， 测试环境http://127.0.5 ， 生产环境http://www.abc.com
console.log(process.env);
if (process.env.NODE_ENV == "development") {
    axios.defaults.baseURL = "http://pcapi-xiaotuxian-front-devtest.itheima.net";
} else if (process.env.NODE_ENV == "production") {
    axios.defaults.baseURL = "http://www.abc.com";
} else {
    axios.defaults.baseURL = "http://test.com";
}
// 4、请求拦截，
axios.interceptors.request.use((config) => {
    //1) 添加loading
    //2) 添加请求头，header, 中的token值
    //3）请求参数的处理 qs
    return config;
});
// 响应拦截
axios.interceptors.response.use((response) => {
    //1）关闭loading
    //2) 各种状态码判断以及相应的逻辑
    return response;
});
export default axios;