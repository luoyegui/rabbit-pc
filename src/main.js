import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
// 引入默认的样式文件
import "/src/styles/common.scss"
// import "/src/styles/var.scss"
import router from './router'
import store from './store'
import Axios from 'axios'
Vue.prototype.axios = Axios
import ElementUI from 'element-ui';
// 修改主题色
import "./theme/element/index.css"
// import 'element-ui/lib/theme-chalk/index.css';

// 图片懒加载
import VueLazyload from 'vue-lazyload'
// Vue.use(VueLazyload) //无配置项
// 配置项
const loadimage = require('/src/assets/images/200.png')
const errorimage = require('/src/assets/logo.png')
Vue.use(VueLazyload, {
  preLoad: 1.3, //预加载的宽高比
  loading: loadimage, //图片加载状态下显示的图片
  error: errorimage, //图片加载失败时显示的图片
  attempt: 1, // 加载错误后最大尝试次数
})
Vue.config.productionTip = false
Vue.use(ElementUI);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
