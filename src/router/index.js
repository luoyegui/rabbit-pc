import Vue, { reactive } from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import Layout from '../views/Layout/index.vue';
import Home from "../views/Home/index.vue";
import Sort from "../views/sort/index.vue";
import Login from "../views/Login/index.vue";
import My from '../views/my/index.vue';
// import { component } from 'vue/types/umd';
import Cart from "../views/cart/index.vue";

// import Home from "../views/Home/index.vue";
const routes = [
    {
        path: '/',
        component: Layout,
        children: [
            // 二级路由或者三级路由中当path中写"/"代表根路径，如果不写自动同上面的路径进行拼接
            {
                path: "",
                component: Home
            },
            ,{
                path: "My",
                component:My
            },
            {
                path: 'cart',
                component: Cart
            },{
                path: "sort",
                component: Sort
            }
        ]
    },
    {
        path: '/login',
        component: Login
    }
];
const router = new VueRouter({
    routes,
    mode: "hash"
})
export default router;